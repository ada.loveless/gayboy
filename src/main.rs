use std::{fmt::Write, io::Read};
use clap::Parser;
use nom::{
  IResult,
  bytes::streaming::{tag, take},
};

enum CommandType {
    Init, // 0x01
    Print, // 0x02
    Data, // 0x04
    Status, // 0x0F
    Unknown, // Others
}

enum CompressionType {
    None, // 0x00
    Unknown, // Others
}

struct Preamble {
    command: CommandType,
    compression: CompressionType,
    data_length: u16,
}

fn parse_preamble(stream: &[u8]) -> IResult<&[u8], Preamble, ()>{
    let (stream, _) = tag::<&[u8], &[u8], ()>(&[0x88, 0x33])(stream)?;
    let (stream, command) = take(1usize)(stream)?;
    let command_type = match command[0] {
        0x01 => CommandType::Init,
        0x02 => CommandType::Print,
        0x04 => CommandType::Data,
        0x0F => CommandType::Status,
        _ => CommandType::Unknown,
    };
    let (stream, compression) = take(1usize)(stream)?;
    let compression_type = match compression[0] {
        0x00 => CompressionType::None,
        _ => CompressionType::Unknown,
    };
    let (stream, data_length) = take(2usize)(stream)?;
    let preamble = Preamble { 
        command: command_type, 
        compression: compression_type, 
        data_length: u16::from_be_bytes([data_length[0], data_length[1]]),
    };
    Ok((stream, preamble))
}

struct Postamble {
    checksum: u16,
    alive: u8,
    status: u8,
}

fn parse_postamble(stream: &[u8]) -> IResult<&[u8], Postamble, ()>{
    let (stream, checksum) = take(2usize)(stream)?;
    let (stream, alive) = take(1usize)(stream)?;
    let (stream, status) = take(1usize)(stream)?;
    let preamble = Postamble { 
        checksum: u16::from_be_bytes([checksum[0], checksum[1]]),
        alive: alive[0],
        status: status[0],
    };
    Ok((stream, preamble))
}

struct Packet {
    preamble: Preamble,
    data: Vec<u8>,
    postamble: Postamble,
}

fn parse_packet(stream: &[u8]) -> IResult<&[u8], Packet, ()> {
    let (stream, preamble) = parse_preamble(stream)?;
    let (stream, data) = take(preamble.data_length)(stream)?;
    let (stream, postamble) = parse_postamble(stream)?;
    let packet = Packet {
        preamble,
        data: data.to_vec(),
        postamble
    };
    Ok((stream, packet))
}

enum Error {
    InvalidCommand,
    InvalidData,
    Truncated,
    IO(std::io::Error),
}

type Result<T> = std::result::Result<T, Error>;

fn try_read(reader: &mut impl Read, mut buffer: &mut [u8]) -> Result<()> {
    match reader.read_exact(&mut buffer) {
        Err(e) => Err(Error::IO(e)),
        Ok(_) => Ok(()),
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Directory to output data in
    #[arg(long, short)]
    out_dir: Option<String>,
}

fn main() {
    let args = Args::parse();
}
